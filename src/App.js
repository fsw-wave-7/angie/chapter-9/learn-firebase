import {BrowserRouter as Router , Switch, Route} from 'react-router-dom'
import Register from './pages/register.page'
import Uploader from './pages/uploader.page'

function App() {
  return (
    <Router>
        <Switch>
          <Route exact path="/">
            <Register />
        </Route>
          <Route path="/upload">
            <Uploader />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
