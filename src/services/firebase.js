import firebase from "firebase/app"
import "firebase/auth" 
import "firebase/storage"

export default firebase.initializeApp({
    apiKey: "AIzaSyDVATggepEoJmnhuk0cYDQUEC4CsFMJ-NY",
    authDomain: "learn-firebase-79110.firebaseapp.com",
    projectId: "learn-firebase-79110",
    storageBucket: "learn-firebase-79110.appspot.com",
    messagingSenderId: "728263388403",
    appId: "1:728263388403:web:ab22d1ca0dc84aac505b86",
    measurementId: "G-6PC2GKYLN5"
})